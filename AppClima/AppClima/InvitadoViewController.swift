//
//  InvitadoViewController.swift
//  AppClima
//
//  Created by Jose Gonzalez on 25/10/17.
//  Copyright © 2017 Jose Gonzalez. All rights reserved.
//

import UIKit

class InvitadoViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

  
    @IBAction func salirButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
