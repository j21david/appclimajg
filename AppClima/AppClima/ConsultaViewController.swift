//
//  ConsultaViewController.swift
//  AppClima
//
//  Created by Jose Gonzalez on 31/10/17.
//  Copyright © 2017 Jose Gonzalez. All rights reserved.
//

import UIKit

class ConsultaViewController: UIViewController {

    
    
    //MARK:- Outlets
    
    @IBOutlet weak var cityTExtField: UITextField!
    
    @IBOutlet weak var weatherLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    @IBAction func consultarButtonPressed(_ sender: Any) {
        let urlStr = "http://api.openweathermap.org/data/2.5/weather?q=\(cityTExtField.text ?? "Quito")&appid=f4b14259b70ff9328f483a7729e0c980"
        
            let url = URL(string: urlStr)
            let request = URLRequest(url: url!)
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
                  if let _ = error {
                        return
                      }
            
                  do {
                
                        let weatherJson = try JSONSerialization.jsonObject(with: data!, options: [])
                    
                    let weatherDict = weatherJson as! NSDictionary
                    print(weatherDict)
                    guard let weatherKey = weatherDict["weather"] as? NSArray else {
                    
                        DispatchQueue.main.async {
                            self.weatherLabel.text = "Ciudad no valida"
                        }
                        return
                    }
                    let weather = weatherKey[0] as! NSDictionary
                    
                    DispatchQueue.main.async {
                          self.weatherLabel.text = "\(weather["description"] ?? "Error")"
                    }
                   
                    print(weather["description"])
                    
                    print("---")
                    print(type(of: weather))
                    
                
                      } catch {
                    
                                print("Error al generar el Json")
                        
                          }
            
            
                }
            
                task.resume()
        
        
    }
    
}
