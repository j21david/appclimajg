//
//  ViewController.swift
//  AppClima
//
//  Created by Jose Gonzalez on 25/10/17.
//  Copyright © 2017 Jose Gonzalez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    //MARK:- Outlets
    
    @IBOutlet weak var usuarioTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    
    // MARK:- ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    
    @IBAction func IngresarButton(_ sender: Any) {
        
        let user = usuarioTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        switch (user,password) {
        case ("jose","123"):
            performSegue(withIdentifier: "ciudadSegue", sender: self)
        
        case ("jose", _):
            print("contraseña incorrecta")
        
        default:
            print("usuario y contraseña incorrecta")
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        usuarioTextField.text = ""
        passwordTextField.text = ""
        usuarioTextField.becomeFirstResponder()
        
    }
    
    
    @IBAction func invitadoButton(_ sender: Any) {
    }
    
}

